﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace ProjectCode
{

    public delegate void ProgressUpdatedEventHandler(object sender, double progress);

    public class FileConversion
    {
        private string originalFile;
        private string resolution;
        private string destinyFile;

        private readonly SynchronizationContext syncContext;

        private double totalDuration = 0;

        private Process p;

        public event ProgressUpdatedEventHandler ProgressUpdated;


        private StringBuilder errorOutputForLogging = new StringBuilder();

        public FileConversion(string originalFile, string resolution)
        {
            this.originalFile = originalFile;
            this.resolution = resolution;
            this.syncContext = AsyncOperationManager.SynchronizationContext;
        }

        protected virtual void OnProgressUpdated(double progress)
        {
            if (ProgressUpdated != null)
            {
                this.ProgressUpdated(this, progress);
            }
        }


        public void startConversion()
        {
            try
            {
                var dir = "e:\\ffmpeg\\";
                var executable = "ffmpeg.exe";

                string target = Path.Combine(dir, executable);

                var fileInfo = new FileInfo(target);
                if (!fileInfo.Exists)
                {
                    throw new FileNotFoundException("The file was not found" + fileInfo.FullName);
                }

                DateTime fileName = DateTime.Today;
                this.destinyFile = fileName.Day.ToString() + "-" + fileName.Month.ToString() + "-" + fileName.Year.ToString();

                string exeToRun = @"e:\ffmpeg\ffmpeg.exe";

                this.destinyFile = @"e:\" + this.destinyFile + ".mp4";

                string arguments = "-y -i " + this.originalFile + " -vcodec libx264 -strict -2 -vb 1200k -acodec aac -ar 48000 -s " + this.resolution + " " + this.destinyFile;

                this.errorOutputForLogging.Append("Command Run: ").Append(exeToRun).Append(" ").AppendLine(arguments).AppendLine();
                this.errorOutputForLogging.AppendLine("############### Standard Error ###############");

                this.p = new Process();
                this.p.StartInfo.FileName = exeToRun;
                this.p.StartInfo.Arguments = arguments;
                this.p.StartInfo.UseShellExecute = false;
                this.p.StartInfo.RedirectStandardError = true;
                this.p.StartInfo.CreateNoWindow = true;

                this.p.ErrorDataReceived += new DataReceivedEventHandler(ErrorDataReceived);

                this.p.Start();

                this.p.BeginErrorReadLine();

                this.p.WaitForExit();
                if (this.p.ExitCode == 0)
                {
                    MailMessenger.sendEmail(this.errorOutputForLogging.ToString());
                    // Send finished event!
                }
                else
                {
                    // Send error event!
                    MailMessenger.sendEmail(this.errorOutputForLogging.ToString());
                }
            }
            catch (Exception e)
            {
                // Something went very wrong because it not even made a conversion!
                MailMessenger.sendEmail(e.Message);
                // Send error event!
            }
            
        }

        private void cleanUpTempFile()
        {
            try
            {
                if (File.Exists(this.destinyFile))
                {
                    //We need to cleanup afterward the kill.
                    File.Delete(this.destinyFile);
                }
            }
            catch (Exception)
            {
            }
        }

        private void ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            string data = e.Data;

            this.errorOutputForLogging.AppendLine(data);
            try
            {
                if (this.totalDuration == 0)
                {
                    // Let's try to match the duration
                    // Duration: 00:01:04.77, start: 0.000000, don't care about ms
                    string pattern = @"(Duration:) \d{2}:\d{2}:\d{2}\.{1,3}";

                    Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = r.Match(data);
                    while (m.Success)
                    {
                        // Let's explode the duration, the value is in the second value
                        string[] durations = m.Value.Split(' ');
                        string[] totalTime = durations[1].Split(':');
                        this.totalDuration = Convert.ToInt32(totalTime[0]) * 3600 + Convert.ToInt32(totalTime[1]) * 60 + Convert.ToDouble(totalTime[2]);

                        m.NextMatch();
                        break;
                    }
                }
                else if (this.totalDuration > 0)
                {
                    // Let's try to match the current time converted
                    // time=00:00:11.50
                    string pattern = @"(time=)\d{2}:\d{2}:\d{2}\.{1,3}";
                    Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match m = r.Match(data);
                    while (m.Success)
                    {
                        // Let's explode the duration, the value is in the second value
                        string[] durations = m.Value.Split('=');
                        string[] totalTime = durations[1].Split(':');
                        double currentDuration = Convert.ToInt32(totalTime[0]) * 3600 + Convert.ToInt32(totalTime[1]) * 60 + Convert.ToDouble(totalTime[2]);


                        double progress = currentDuration / this.totalDuration * 100;

                        if (progress <= 100)
                        {
                            this.syncContext.Post(delegate
                            {
                                this.OnProgressUpdated(progress);
                            }, null);
                        }
                        m.NextMatch();
                        break;
                    }
                }
            }
            catch (Exception)
            {
                // Do nothing
            }
        }

        public void RequestStop()
        {
            if (!this.p.HasExited)
            {
                this.p.Kill();
                try 
	            {
                    this.p.WaitForExit();
	            }
	            catch (Exception)
	            {
                    // Do nothing
	            }
                this.cleanUpTempFile();
            }
        }
    }
}
