﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCode
{
    public static class MailMessenger
    {
        public static void sendEmail(string content)
        {
            try {
                // Do nothing for now
                throw new Exception();


                MailMessage mailMsg = new MailMessage();
                mailMsg.To.Add(new MailAddress("s.diniz@videobserver.com", "Sérgio Diniz"));
                mailMsg.From = new MailAddress("info@videobserver.com", "VO Encoder");

                mailMsg.Subject = "VO Encoder Exception";

                content = content.Replace(Environment.NewLine, "<br />");

                string html = @"<p>" + content + "</p>";

                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, Encoding.UTF8, MediaTypeNames.Text.Html));

                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));

                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("user", "password");
                smtpClient.Credentials = credentials;

                // No credentials no email
                smtpClient.Send(mailMsg);
            } catch ( Exception) {
                // No email could be sent so do nothing, this should probably be logged to an sql lite database,
                // And when there is net we could sent id
            }
        }
    }
}
