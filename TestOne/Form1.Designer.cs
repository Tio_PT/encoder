﻿namespace TestOne
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectFiles = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.cancelConvertion = new System.Windows.Forms.Button();
            this.ProgressPercentage = new System.Windows.Forms.Label();
            this.ResolutionSelector = new System.Windows.Forms.ComboBox();
            this.listFiles = new System.Windows.Forms.ListView();
            this.startConversions = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectFiles
            // 
            this.selectFiles.Location = new System.Drawing.Point(973, 29);
            this.selectFiles.Name = "selectFiles";
            this.selectFiles.Size = new System.Drawing.Size(121, 23);
            this.selectFiles.TabIndex = 0;
            this.selectFiles.Text = "Open File";
            this.selectFiles.UseVisualStyleBackColor = true;
            this.selectFiles.Click += new System.EventHandler(this.selectFiles_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "All Video Types|*.mp4;*.avi;*.vob;";
            this.openFileDialog1.Multiselect = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(21, 367);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(912, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // cancelConvertion
            // 
            this.cancelConvertion.Location = new System.Drawing.Point(961, 367);
            this.cancelConvertion.Name = "cancelConvertion";
            this.cancelConvertion.Size = new System.Drawing.Size(121, 23);
            this.cancelConvertion.TabIndex = 4;
            this.cancelConvertion.Text = "Cancel";
            this.cancelConvertion.UseVisualStyleBackColor = true;
            this.cancelConvertion.Click += new System.EventHandler(this.cancelConvertion_Click);
            // 
            // ProgressPercentage
            // 
            this.ProgressPercentage.AutoSize = true;
            this.ProgressPercentage.Location = new System.Drawing.Point(471, 393);
            this.ProgressPercentage.Name = "ProgressPercentage";
            this.ProgressPercentage.Size = new System.Drawing.Size(21, 13);
            this.ProgressPercentage.TabIndex = 5;
            this.ProgressPercentage.Text = "0%";
            // 
            // ResolutionSelector
            // 
            this.ResolutionSelector.FormattingEnabled = true;
            this.ResolutionSelector.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.ResolutionSelector.Location = new System.Drawing.Point(973, 71);
            this.ResolutionSelector.Name = "ResolutionSelector";
            this.ResolutionSelector.Size = new System.Drawing.Size(121, 21);
            this.ResolutionSelector.TabIndex = 6;
            // 
            // listFiles
            // 
            this.listFiles.Location = new System.Drawing.Point(12, 12);
            this.listFiles.Name = "listFiles";
            this.listFiles.Size = new System.Drawing.Size(921, 349);
            this.listFiles.TabIndex = 7;
            this.listFiles.UseCompatibleStateImageBehavior = false;
            this.listFiles.View = System.Windows.Forms.View.List;
            // 
            // startConversions
            // 
            this.startConversions.Location = new System.Drawing.Point(973, 117);
            this.startConversions.Name = "startConversions";
            this.startConversions.Size = new System.Drawing.Size(121, 23);
            this.startConversions.TabIndex = 8;
            this.startConversions.Text = "Start Conversion";
            this.startConversions.UseVisualStyleBackColor = true;
            this.startConversions.Click += new System.EventHandler(this.startConversions_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(1181, 415);
            this.Controls.Add(this.startConversions);
            this.Controls.Add(this.listFiles);
            this.Controls.Add(this.ResolutionSelector);
            this.Controls.Add(this.ProgressPercentage);
            this.Controls.Add(this.cancelConvertion);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.selectFiles);
            this.Name = "Form1";
            this.Text = "VO Encoder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectFiles;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button cancelConvertion;
        private System.Windows.Forms.Label ProgressPercentage;
        private System.Windows.Forms.ComboBox ResolutionSelector;
        private System.Windows.Forms.ListView listFiles;
        private System.Windows.Forms.Button startConversions;
    }
}

