﻿using ProjectCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TestOne
{
    public partial class Form1 : Form
    {

        private Thread conversionThread = null;
        private FileConversion fc = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Let's load the several options to resolution selector
            ResolutionSelector.Items.Add("720x480");
            ResolutionSelector.Items.Add("1280x720");
            ResolutionSelector.Items.Add("1920x1080");
            ResolutionSelector.AutoCompleteMode = AutoCompleteMode.None;
            ResolutionSelector.DropDownStyle = ComboBoxStyle.DropDownList;
            ResolutionSelector.SelectedIndex = 0;
        }

        private void updateProgress(object sender, double progress)
        {
            progressBar1.Value = Convert.ToInt32(progress);
            ProgressPercentage.Text = Convert.ToInt32(progress).ToString() + "%";
        }

        private void selectFiles_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                foreach (String file in openFileDialog1.FileNames)
                {
                    listFiles.Items.Add(file);
                }
            }
        }

        private void cancelConvertion_Click(object sender, EventArgs e)
        {
            if (this.conversionThread != null)
            {
                this.fc.RequestStop();
                this.conversionThread.Abort();
                this.progressBar1.Value = 0;
                ProgressPercentage.Text = "";
                this.selectFiles.Enabled = true;
            }
        }

        private void startConversions_Click(object sender, EventArgs e)
        {
            this.selectFiles.Enabled = false;
            //Queue<string> filesQueue = new Queue<string>();

            //foreach (ListViewItem item in listFiles.Items)
            //{
            //    filesQueue.Enqueue(item.Text);
            //}

            //ConversionManager cm = new ConversionManager(filesQueue, ResolutionSelector.SelectedItem.ToString());

            //cm.startConversion();

            foreach (ListViewItem item in listFiles.Items)
            {
                string sd = Application.StartupPath;

                this.fc = new FileConversion(item.Text, ResolutionSelector.SelectedItem.ToString());

                this.fc.ProgressUpdated += new ProgressUpdatedEventHandler(this.updateProgress);

                this.conversionThread = new Thread(new ThreadStart(this.fc.startConversion));

                this.conversionThread.Start();

                while (!this.conversionThread.IsAlive) ;
                break;
            }
        }
    }
}
